/**
 *  qsystem.js
 *  Use Linux System features in NodeJS
 *
 **/

 const EventEmitter = require('events').EventEmitter;
 const fs = require('fs');
 const util = require('util');
 const path = require('path');
 const { exec } = require('child_process');

 util.inherits(Qsystem, EventEmitter);
 module.exports = Qsystem;

 function Qsystem(){
   const defaultShutdownTime = 3;       // default timeout for shutdown/reboot

   this.shutdown = shutdown;
   this.reboot = reboot;

   // shutdown the system in <seconds> seconds
   function shutdown( seconds ){
     var timeout = defaultShutdownTime;
     if( seconds != 'undefined' && !isNaN( seconds ) ) timeout = seconds; // check if a timeout in seconds is defined
     console.debug('shutting down in '+timeout+' seconds!');
     this.emit( 'shutdown', timeout );                        // notify event listeners
     setTimeout(function(){
       console.debug('shutting down now!');
       exec('shutdown -h now');
     }, timeout*1000);
   }

   // reboot the system in <seconds> seconds
   function reboot( seconds ){
     var timeout = defaultShutdownTime;
     if( seconds != 'undefined' && !isNaN( seconds ) ) timeout = seconds; // check if a timeout in seconds is defined
     console.debug('rebooting in '+timeout+' seconds!');
     this.emit( 'reboot', timeout );                        // notify event listeners
     setTimeout(function(){
       console.debug('rebooting now!');
       exec('shutdown -r now');
     }, timeout*1000);
   }
 }
