# QsystemJS
Use Linux System features in your NodeJS project

:warning: **Linux-based systems only! (e.g. your Raspberry Pi, or your Ubuntu workstation)**

## System functions
Functions available
```
.on( event, function)

.shutdown( timeout )
.reboot( timeout )
```
Events
```
shutdown <timeout>
reboot <timeout>
```

## Collaborate
Don't hesitate to contact the project maintainer(s)!
